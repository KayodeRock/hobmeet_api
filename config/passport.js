var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var bycrypt = require('bcrypt-nodejs');

passport.use(new localStrategy(function(username, password, cb){
    User.findOne({email: username}, function(err, user){

        if(err) return cb(err);
        if(!user) return cb(null, false, {message:"Authentication failed mail"});
            
        bycrypt.compare(password, user.password, function(err, res){
            if(!res){
                return cb(null, false, {message:"Authenticated failed pass"});
            }
            return cb(null, user, {message:"Authentication Successful"});
        })
          
    });
}));