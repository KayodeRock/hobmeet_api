/**
 * Hobby.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection : 'userMongodbServer',
  schema: true,
  attributes: {
    user_id:{
      type:'string',
      required: 'true'
    },
    hobby:{
      type:'string',
      required: true
    },
    toJSON: function(){
      let user = this.toObject();
      delete user.user_id;
      return user;
    }
  }
};

