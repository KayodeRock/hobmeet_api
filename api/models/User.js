/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var bcrypt = require('bcrypt-nodejs');

module.exports = {
  connection : 'userMongodbServer',

  schema: true,

  attributes: {
    email:{
      type:'string',
      unique:'true',
      required: 'true'
    },
    password:{
      type:'string',
      required: 'true'
    },
    firstname:{
      type:'string',
      required: 'true'
    },
    lastname:{
      type:'string',
      required: 'true'
    },
    state:{
      type:'string'
    },
    country:{
      type:'string'
    },
    phone:{
      type:'string'
    },
    age:{
      type:'string'
    },
    gender:{
      type:'string'
    },
    toJSON: function(){
      let user = this.toObject();
      delete user.password;
      return user;
    }
  },
  
  beforeCreate: function(user, cb){
    bcrypt.genSalt(10, function(err, salt){
      bcrypt.hash(user.password, salt, null, function(err, hash){
        if(!err){
           user.password = hash;
        }else{
          console.error(err);
        }
        cb();
      });
    });
  }

};  

