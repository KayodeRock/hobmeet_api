/**
 * HobbyController
 *
 * @description :: Server-side logic for managing hobbies
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	getHobbies(req, res){
        Hobby.find({user_id:req.param("user_id")}).exec(function (err, hobbies){
            if (err) {
                return res.serverError(err);
            }
            return res.json(hobbies);
        });
    },
    
    create(req, res){
        Hobby.create({user_id:req.param("user_id"), hobby:req.param("hobby")}).exec(function (err, hobby){
            if (err) {
                return res.serverError(err);
            }

            User.findOne({id:req.param("user_id")}).exec(function (err, user){
                if (err || !user) {
                    return res.serverError(err);
                }
                
                let message = "Congrats! "+req.param("hobby")+" was just added to your hobby list.";

                // Send mail & messages here!
                MailService.sendEmail({ subject:"HobMeet - New Hobby Added", 
                                        message, email:user.email});

                if(user.phone != ""){
                    SMSService.sendSMS({phone:user.phone, message})
                }

            });

            return res.json(hobby);
        });      
        
    },

    deleteHobby(req, res){
        Hobby.destroy({id:req.param("id")}).exec(function (err, hobby){
            if (err) {
                return res.serverError(err);
            }

            User.findOne({id:req.param("user_id")}).exec(function (err, user){
                if (err || !user) {
                    return res.serverError(err);
                }

                let message = "Ooops! "+req.param("hobby")+" was just deleted from your hobby list.";

                // Send mail & messages here!
                MailService.sendEmail({ subject:"HobMeet - Hobby deleted", 
                                        message, email:user.email});

                if(user.phone != ""){
                    SMSService.sendSMS({phone:user.phone, message})
                }

            });

            return res.json(hobby);
        }); 
    }

};

