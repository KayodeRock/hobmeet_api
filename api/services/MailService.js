const SparkPost = require('sparkpost');
const sparkPostSender = new SparkPost(process.env.SPARKPOST_KEY)

module.exports = {
  sendEmail (vars) {
    sparkPostSender.transmissions.send({
       
        content: {
          from: 'kb@lanre.co',
          subject: vars.subject,
          html:'<html><body><p>'+ vars.message +'</p></body></html>'
        },
        recipients: [
          {address: vars.email}
        ]
      })
      .then(data => {
        sails.log('Email sent!');
        sails.log(data);
      })
      .catch(err => {
        sails.log('Error sending message');
        sails.log(err);
      });
  }
};