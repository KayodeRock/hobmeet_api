const twilio = require('twilio');
const accountSid = process.env.TWILIO_SID
const authToken = process.env.TWILIO_AUTH_TOKEN

var client = new twilio(accountSid, authToken);
module.exports = {
    sendSMS(vars){
        client.messages.create({
            to:vars.phone, 
            from:'+16263250829',
            body: vars.message
        }, function(error, message) {
            if (!error) {
                sails.log(message.dateCreated);
            } else {
                sails.log('Oops! There was an error.');
            }
        });
    }

}